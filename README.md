# python mysql

#### 介绍
首先声明： python 新手一枚 :grimacing: 

手上有个小项目，需要操作 MYSQL 数据库，找了 SQLAlchemy 感觉学习成本有点高，使用 pymysql 裸奔一段时间之后 代码很混乱不说 还容易出错。网上找了一圈好像也没有发现现成的类。

于是，就有这个基于 pymysql 的【增】【删】【改】【查】基础小轮子，简单粗暴 ，没有太多的功能 ，在自己的小项目上跑了一段时间，自我感觉很好用，分享给大家做简单小任务时使用~_~ 

#### 安装教程

1. pip install pymysql
2. import mysql
2. 开始使用

#### 使用说明

##### 初始化

```
t = Db('table',{'prefix':'your_prefix','host':'your_host','port':'your_port','user':'your_user','password':'your_password','database':'your_database'})
```

##### 增

```
addLastRowId = t.add({'age':1,'name':'JESSE'})
addRows = t.addAll([{'age':1,'name':'JESSE'},{'age':2,'name':'JESSE2'}])
```

##### 删
```
deleteRow = t.delete({'id':1})
```
##### 改
```
changeRow = t.save({'id':1,'name':'JESSE'})
```
##### 查
```
dataRows = t.select(where='id>1',field='id,name',limit='2,3',order='id DESC')
dataRow = t.find(where='id=1',field='id,name')
fieldValue = t.getField(field='name',where="id = 1")
```
##### 链式混查
```
result = t.table('west_zy',True).limit(1).where('id < 100').order('id DESC').field('name').select(where="id>10")
```
##### 执行SQL
```
reslut = t.query("SELECT * FROM west_zy")
```
##### 查看SQL
```
sql = t.getsql()
t.getsql().select(where="id>10")
```
##### 其它
默认表主键名为 ID，传 data 时自动检查主键，可以省略 where 参数


#### 后记

1. 项目没倒闭之前我会一直更新
2. PHP过来的小白，风格有点类似 THINKPHP 大牛请勿介意
3. 我是 JESSE 本职SEO，着迷于学习编程 很想认识你，欢迎联系我：818seo@gmail.com